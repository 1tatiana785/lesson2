import java.util.ArrayList;

abstract class Animals {
    protected String name;
    protected ArrayList<String> opportunities;
    protected int health;

    abstract protected void setName();

    abstract protected void setHealth();

    abstract protected void addOpportunities();

    protected void setOpportunities() {
        opportunities = new ArrayList<String>();
        opportunities.add("eat");
        opportunities.add("sleep");
        opportunities.add("play");
    }

    public String getName() {
        return name;
    }

    public int getHealth() {
        return health;
    }

    public ArrayList<String> getOpportunities() {
        return opportunities;
    }

    public boolean eat() {
        health++;
        System.out.println("I am eating");
        return isDead();
    }

    public boolean sleep() {
        health++;
        System.out.println("I am sleeping");
        return isDead();
    }

    public boolean play() {
        health--;
        System.out.println("I am playing");
        return isDead();
    }

    public boolean isDead() {
        if (health > 0) {
            return true;
        } else {
            System.out.println(name + " is at hospital");
            return false;
        }
    }
}