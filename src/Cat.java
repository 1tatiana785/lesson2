public class Cat extends Animals {

    Cat() {
        setName();
        setHealth();
        setOpportunities();
        addOpportunities();
    }

    protected void setName() {
        this.name = "Fedor";
    }

    protected void setHealth() {
        this.health = 5;
    }

    protected void addOpportunities() {
        this.opportunities.add("purr"); // мурчать
    }

    public void purr() {
        health--;
        System.out.println("I am purring");
        isDead();
    }
}