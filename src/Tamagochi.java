import java.util.ArrayList;
import java.util.Scanner;
import java.lang.reflect.*;

public class Tamagochi {
    private String name;
    private int health = 0;

    private Tamagochi() {
        String[] name = new String[]{"Cat", "Dog"};
        System.out.println("Choice an animal");
        for (int i = 0; i < name.length; i++) {
            System.out.println(name[i] + " : " + i);
        }
        Scanner scan = new Scanner(System.in);
        String currentAnimal = scan.next();
        switch (currentAnimal) {
            case "0":
                Cat cat = new Cat();
                this.name = cat.getName();
                this.health = cat.getHealth();
                initAnimal();
                try {
                    doSomethingCat(cat);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                break;
            case "1":
                Dog dog = new Dog();
                this.name = dog.getName();
                this.health = dog.getHealth();
                initAnimal();
                try {
                    doSomethingDog(dog);
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("You don`t have a pat");
        }
        if (health == 0) new Tamagochi();
    }

    private void doSomethingCat(Cat animal) throws NoSuchMethodException {
        System.out.println("I can");
        ArrayList<String> opportunities = animal.getOpportunities();
        String doAction = action(opportunities);
        Class c = animal.getClass();
        Method m = c.getMethod(doAction);
        try {
            Object o = m.invoke(animal);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        this.health = animal.getHealth();
        if (this.health > 0) {
            this.health = animal.getHealth();
            System.out.println("My health is " + this.health + " points");
            doSomethingCat(animal);
        } else {
            new Tamagochi();
        }
    }

    private void doSomethingDog(Dog animal) throws NoSuchMethodException {
        System.out.println("I can");
        ArrayList<String> opportunities = animal.getOpportunities();
        String doAction = action(opportunities);
        Class c = animal.getClass();
        Method m = c.getMethod(doAction);
        try {
            Object o = m.invoke(animal);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        this.health = animal.getHealth();
        if (this.health > 0) {
            System.out.println("My health is " + this.health + " points");
            doSomethingDog(animal);
        } else {
            new Tamagochi();
        }
    }

    private String action(ArrayList<String> opportunities) {
        for (int i = 0; i < opportunities.size(); i++) {
            System.out.println(opportunities.get(i) + " : " + i);
        }
        System.out.println("Make your choice");
        Scanner scan = new Scanner(System.in);
        String action = scan.next();

        return opportunities.get(Integer.parseInt(action));
    }

    public static void main(String[] args) {
        new Tamagochi();
    }

    private void initAnimal() {
        System.out.println("My name is " + this.name);
        System.out.println("My health is " + this.health + " points");
    }
}
