public class Dog extends Animals {

    Dog() {
        setName();
        setHealth();
        setOpportunities();
        addOpportunities();
    }

    protected void setName() {
        this.name = "Sharik";
    }

    protected void setHealth() {
        this.health = 10;
    }

    protected void addOpportunities() {
        this.opportunities.add("bark"); // лаять
    }

    public void bark() {
        health--;
        System.out.println("I am barking");
        isDead();
    }
}